package com.mem.controller;
//package com.mem.controller;
//
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.List;
//
//import javax.servlet.ServletException;
//import javax.servlet.http.HttpServlet;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import com.mem.model.MemService;
//import com.mem.model.MemVO;
//
//public class LoginHandler extends HttpServlet{
//
//	MemService memSvc = new MemService();
//	MemVO memVO = memSvc.getOneMem(memno);
//	
//	protected boolean allowuser(String account, String password){
//		
//		if(memVO.getMemid().equals(account) && memVO.getMempassword().equals(password)){
//			return true;
//		}else{
//			return false;
//		}
//	}
//	
//	@Override
//	protected void doGet(HttpServletRequest req, HttpServletResponse res)
//			throws ServletException, IOException {
//		doPost(req, res);
//	}
//
//	@Override
//	protected void doPost(HttpServletRequest req, HttpServletResponse res)
//			throws ServletException, IOException {
//		
//		req.setCharacterEncoding("UTF-8");
//		PrintWriter out = res.getWriter();
//		
//		String account = req.getParameter("account");
//		String password = req.getParameter("password");
//		
//		if(!allowuser(account, password)){  //【帳號 , 密碼無效時
//			if(!memVO.getMemid().equals(account) && !memVO.getMempassword().equals(password)){
//				out.println("<HTML><HEAD><TITLE>Access Denied</TITLE></HEAD>");
//				out.println("<BODY>無效的使用者帳戶!<BR>");
//				out.println("請按此重新登入 <A HREF="+req.getContextPath()+"/login.html>重新登入</A>");
//				out.println("</BODY></HTML>");
//		    
//			}else if(!memVO.getMemid().equals(account) || !memVO.getMempassword().equals(password)) {
//				out.println("<HTML><HEAD><TITLE>Access Denied</TITLE></HEAD>");
//			    out.println("<BODY>帳號密碼錯誤 請重新輸入!<BR>");
//				out.println("請按此重新登入 <A HREF="+req.getContextPath()+"/login.html>重新登入</A>");
//			    out.println("</BODY></HTML>");
//			}
//		}else{                              //帳號 密碼有效時, 才做以下工作
//			HttpSession session = req.getSession();
//			session.setAttribute("account", account);
//			
//			try{
//				String location = (String)session.getAttribute("location");
//				if(location != null){
//					session.removeAttribute(location);
//					res.sendRedirect(location);
//					return;
//				}
//			}catch(Exception ignored){	}
//			
//			res.sendRedirect("/select_page.jsp");           //*工作3: (如無來源網頁, 則重導至select_page.jsp網頁)
//		}
//		
//	}
//
//}
