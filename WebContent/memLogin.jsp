<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="BIG5">
<title>memLogin.jsp</title>
</head>
<body>

<form method="post" action="<%= request.getContextPath()%>/mem/LoginHandler.do" >
<center>
<table border="1">
	<tr>
		<td colspan="2">
		<p align="center">Welcome!<br>
						  Please enter your Account Number,<br>
						  Password to log in.</p>
		</td>
	</tr>

	<tr>
		<td><p align="right"><b>Account:</b></p></td>
		<td><p><input type="text" name="memid" value="" size="15"></p></td>	
	</tr>
	
	<tr>
		<td><p align="right"><b>Password:</b></p></td>
		<td><p><input type="password" name="mempassword" value="" size="15"></p></td>
	</tr>

	<tr>
		<td colspan="2"><center><input type="submit" value="Ok"></center></td>
		
	</tr>

</center>
</table>

</form>
</body>
</html>