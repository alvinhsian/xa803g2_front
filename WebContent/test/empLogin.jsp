<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="BIG5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta charset="UTF-8">
<LINK rel="SHORTCUT ICON" href="<%=request.getContextPath()%>/images/icon_1r_48.png">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.9.1.min.js"></script>
<title>empLogin.jsp</title>
<style type="text/css">
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	-moz-background-size: cover;
	background-size: cover;
	background-color: #000;
	background-image: url(<%=request.getContextPath()%>/images/bg1.jpg);
	background-repeat: no-repeat;
	background-position: center center;
	}
.index {
	/* [disabled]background-image: url(./bg1.jpg); */
	height: 1100px;
	width: 960px;
	margin-right: auto;
	margin-left: auto;
	position: relative;
	}
#login {
	position: absolute;
	height: 250px;
	width: 208px;
	left: 394px;
	top: 231px;
	}
#logo {
	position: absolute;	
	height: 103px;
	width: 189px;
	left: 385px;
	top: 20px;
	}
.idInput {
	border-radius: 20px;
	outline: none;
	width: 100px;
	height: 15px;
	}
</style>
</head>
<body>

	<div class="index">
	<div id="logo"><img src="<%= request.getContextPath()%>/images/0519_logo.png" width="200" height="200"></div>
  	<div id="login">
<form method="post" action="<%= request.getContextPath()%>/LoginHandler.do">
  	<table width="100%">
    <tbody>
		<tr>
			<td height="39" align="center"><font color="#000000">員工登入</font>
			</td>
		</tr>
		<tr>
			<td height="30"><font color="#000000">帳號：</font>
			<input type="text" class="idInput" size="15" name="memid" id="memid" value="";">
			</td>
		</tr>
		
		<tr>
			<td height="40"><font color="#000000">密碼：</font>
			<input type="password" class="idInput" size="15" name="mempassword" id="mempassword" value="" ;">
			</td>
		</tr>
    
        <tr>
        	<td height="40" align="center"><span id="checkRes"></span><br>
        	<input id="loginBtn" name="按鈕" type="submit" value="登入">
		    <input id="Reset" name="Reset" type="reset" value="清除">
		    </td>
		</tr>
    </tbody>
    </table>
    
</form>
<div id="msg_user_name"></div>
  	</div>
  
<br clear="all">
</div>

</body>
</html>