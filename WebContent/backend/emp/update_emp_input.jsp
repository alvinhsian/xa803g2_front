<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.emp.model.*"%>
<%@ page import="java.util.*"%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />

<%
	EmployeeVO employeeVO = (EmployeeVO) request.getAttribute("employeeVO"); //EmpServlet.java (Concroller), 存入req的empVO物件 (包括幫忙取出的empVO, 也包括輸入資料錯誤時的empVO物件)
	PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4009);
	List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>
<html>
<head>
<title>員工資料修改 - update_emp_input.jsp</title>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<%-- <script src="<%=request.getContextPath()%>/menu.js"></script>	 --%>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.3/themes/smoothness/jquery-ui.css" />
	<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
	<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script>
$(function() {
	
	$(".datepicker3").datepicker({
		defaultDate:(new Date()),
		dateFormat:"yy-mm-dd",
		showOn : "button",
		buttonImage : "images/calendar.gif",
		buttonImageOnly : true,
		yearRange:"-90:+0",
		changeMonth: true,
		changeYear: true
	});
});

$(function(){
	$('#menu').children('li').click(function(){
		$('#menu').find('ul').removeClass('active');
		$(this).find('ul').toggleClass('active');	
	});
});
</script>
</head>


<body bgcolor='white'>
<c:if test="<%=listPower.contains(powVO)%>"> 
			<%@ include file="/menu1.jsp" %> 
	
	<table border='1' cellpadding='5' cellspacing='0' width='400'>
		<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
			<td>
				<h3>員工資料修改 - update_emp_input.jsp</h3> <a href="<%=request.getContextPath()%>/backend/emp/select_page.jsp"><img
					src="<%=request.getContextPath()%>/emp/images/back1.gif" width="100" height="32" border="0">回首頁</a>
			</td>
		</tr>
	</table>

	<h3>資料修改:</h3>
	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>

	<FORM METHOD="post" ACTION="emp.do" name="form1" enctype="multipart/form-data">
		<table border="0">
			<tr>
				<td>員工編號:<font color=red><b>*</b></font></td>
				<td><%=employeeVO.getEmpNo()%></td>
			</tr>
			<tr>
				<td>員工姓名:</td>
				<td><input type="TEXT" name="empName" size="45"
					value="<%=employeeVO.getEmpName()%>" /></td>
			</tr>
			<tr>
				<td>出生年月日:</td>
				<td>
				<input type = "text" name="empBirth" class = "datepicker3" 
					value="<%=employeeVO.getEmpBirth()%>" /> <a alt="開始日期">
				</a>
				</td>
			</tr>
			<tr>
				<td>電話:</td>
				<td><input type="TEXT" name="empTel" size="45"
					value="<%=employeeVO.getEmpTel()%>" /></td>
			</tr>
			<tr>
				<td>性別:</td>
				<td><input type="TEXT" name="empSex" size="45"
					value="<%=employeeVO.getEmpSex()%>" /></td>
			</tr>
			<tr>
				<td>職稱:</td>
				<td><input type="TEXT" name="empPos" size="45"
					value="<%=employeeVO.getEmpPos()%>" /></td>
			</tr>
			<tr>
				<td>薪資:</td>
				<td><input type="TEXT" name="empSalary" size="45"
					value="<%=employeeVO.getEmpSalary()%>" /></td>
			</tr>
			<tr>
				<td>到職日:</td>
				<td>
					<input type = "text" class = "datepicker3" name="empArrDate" value="<%=employeeVO.getEmpArrDate()%>" />
					
				</td>
			</tr>
			<tr>
				<td>離職日:</td>
				<td>
					<input type = "text" class = "datepicker3" name="empOff" value="<%=employeeVO.getEmpOff()%>"  />
				</td>
			</tr>
			<tr>
				<td>身分證字號:</td>
				<td><input type="TEXT" name="empID" size="45"
					value="<%=employeeVO.getEmpID()%>" /></td>
			</tr>
			<tr>
				<td>住址:</td>
				<td><input type="TEXT" name="empAdd" size="45"
					value="<%=employeeVO.getEmpAdd()%>" /></td>
			</tr>
			<tr>
				<td>照片:</td>
				<td><input type="file" name="empPic" size="45"
					value="<%=employeeVO.getEmpPic()%>" /></td>
			</tr>
			<tr>
				<td>密碼:</td>
				<td><input type="password" name="empPassword" value="<%=employeeVO.getEmpPassword()%>" /></td>
			</tr>
			<tr>
				<td>E-mail:</td>
				<td><input type="TEXT" name="empEmail" value="<%=employeeVO.getEmpEmail()%>" /></td>
			</tr>

		</table>
		<br> 
		<input type="hidden" name="action" value="update"> 
		<input type="hidden" name="empNo" value="<%=employeeVO.getEmpNo()%>">
		<input type="hidden" name="requestURL" value="<%=request.getParameter("requestURL")%>">
		<input type="hidden" name="whichPage" value="<%=request.getParameter("whichPage")%>">
		<input type="submit" value="送出修改">
	</FORM>
				<%@ include file="/menu2.jsp" %> 
	
</c:if>
</body>
</html>
