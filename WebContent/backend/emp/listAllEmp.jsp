<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="java.util.*"%>
<%@ page import="com.emp.model.*"%>
<%-- 此頁練習採用 EL 的寫法取值 --%>
<%@ page import="java.util.*"%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />
<%
    EmployeeService empSvc = new EmployeeService();
    List<EmployeeVO> list = empSvc.getAll();
    pageContext.setAttribute("list",list);
    
	PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4009);
	List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>

<html>
<head>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>	

<title>所有員工資料 - listAllEmp.jsp</title>
</head>
<body bgcolor='white'>

<%@ include file="/menu1.jsp" %> 
<c:if test="<%=listPower.contains(powVO)%>"> 

<b><font color=red>此頁練習採用 EL 的寫法取值:</font></b>
<table border='1' cellpadding='5' cellspacing='0' width='800'>
	<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
		<td>
		<h3>所有員工資料 - ListAllEmp.jsp</h3>
		<a href="select_page.jsp"><img src="images/back1.gif" width="100" height="32" border="0">回首頁</a>
		</td>
	</tr>
</table>

<%-- 錯誤表列 --%>
<c:if test="${not empty errorMsgs}">
	<font color='red'>請修正以下錯誤:
	<ul>
		<c:forEach var="message" items="${errorMsgs}">
			<li>${message}</li>
		</c:forEach>
	</ul>
	</font>
</c:if>

<table border='1' bordercolor='#CCCCFF' width='800'>
	<tr>
		<th>員工編號</th>
		<th>員工姓名</th>
		<th>出生年月日</th>
		<th>電話</th>
		<th>性別</th>
		<th>職稱</th>
		<th>薪資</th>
		<th>到職日</th>
		<th>離職日</th>
		<th>身分證字號</th>
		<th>住址</th>
		<th>照片</th>
		<th>修改</th>
		<th>刪除</th>
	</tr>
	
	<%@ include file="pages/page1.file" %> 
	<c:forEach var="employeeVO" items="${list}" begin="<%=pageIndex%>" end="<%=pageIndex+rowsPerPage-1%>">
		<tr align='center' valign='middle'>
			<td>${employeeVO.empNo}</td>
			<td>${employeeVO.empName}</td>
			<td>${employeeVO.empBirth}</td>
			<td>${employeeVO.empTel}</td>
			<td>${employeeVO.empSex}</td>
			<td>${employeeVO.empPos}</td>
			<td>${employeeVO.empSalary}</td>
			<td>${employeeVO.empArrDate}</td>
			<td>${employeeVO.empOff}</td>
			<td>${employeeVO.empID}</td>
			<td>${employeeVO.empAdd}</td>
			<td><img src="DBGifReader3?empNo=${employeeVO.empNo}"></td>
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/emp/emp.do">
			     <input type="submit" value="修改">
			     <input type="hidden" name = "requestURL" value="<%=request.getRequestURI()%>">
			     
			     <input type="hidden" name="empNo" value="${employeeVO.empNo}">
			     <input type="hidden" name="action"	value="getOne_For_Update"></FORM>
			</td>
			<td>
			  <FORM METHOD="post" ACTION="<%=request.getContextPath()%>/emp/emp.do">
			    <input type="submit" value="刪除">
			    <input type="hidden" name="empNo" value="${employeeVO.empNo}">
			    <input type="hidden" name="action"value="delete"></FORM>
			</td>
		</tr>
	</c:forEach>
</table>
<%@ include file="pages/page2.file" %>

</c:if>
<%@ include file="/menu2.jsp" %> 

</body>
</html>
