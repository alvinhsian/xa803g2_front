<%@ page contentType="text/html; charset=UTF-8" pageEncoding="Big5"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="com.emp.model.*"%>

<%@ page import="java.util.*"%>
<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />
<%
	EmployeeVO employeeVO = (EmployeeVO) request.getAttribute("employeeVO");
	PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4009);
	List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>

<html>
<head>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>	

<title>員工資料新增 - addEmp.jsp</title>
</head>
<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/emp/pages/calendar.css">
<script language="JavaScript" src="<%=request.getContextPath()%>/emp/pages/calendarcode.js"></script>
<div id="popupcalendar" class="text"></div>

<body bgcolor='white'>
<%@ include file="/menu1.jsp" %> 
<c:if test="<%=listPower.contains(powVO)%>"> 

	<table border='1' cellpadding='5' cellspacing='0' width='400'>
		<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
			<td>
				<h3>員工資料新增 - addEmp.jsp</h3>
			</td>
			<td><a href="select_page.jsp"><img src="<%=request.getContextPath()%>/emp/images/tomcat.gif"
					width="100" height="100" border="1">回首頁</a></td>
		</tr>
	</table>

	<h3>資料員工:</h3>
	<%-- 錯誤表列 --%>
	<c:if test="${not empty errorMsgs}">
		<font color='red'>請修正以下錯誤:
			<ul>
				<c:forEach var="message" items="${errorMsgs}">
					<li>${message}</li>
				</c:forEach>
			</ul>
		</font>
	</c:if>

	<FORM METHOD="post" ACTION="emp.do" name="form1" enctype="multipart/form-data">
		<table border="0">

			<tr>
				<td>員工姓名:</td>
				<td><input type="TEXT" name="empName" size="45"
					value="<%=(employeeVO == null) ? "KING" : employeeVO.getEmpName()%>" /></td>
			</tr>
			<tr>
				<%
					java.sql.Date date_SQL = new java.sql.Date(
							System.currentTimeMillis());
				%>
				<td>出生年月日:</td>
				<td bgcolor="#CCCCFF"><input class="cal-TextBox"
					onFocus="this.blur()" size="9" readonly type="text" name="empBirth"
					value="<%=(employeeVO == null) ? date_SQL : employeeVO
					.getEmpBirth()%>">
					<a class="so-BtnLink" href="javascript:calClick();return false;"
					onmouseover="calSwapImg('BTN_date', 'img_Date_OVER',true);"
					onmouseout="calSwapImg('BTN_date', 'img_Date_UP',true);"
					onclick="calSwapImg('BTN_date', 'img_Date_DOWN');showCalendar('form1','empBirth','BTN_date');return false;">
						<img align="middle" border="0" name="BTN_date"
						src="<%=request.getContextPath()%>/emp/images/btn_date_up.gif" width="22" height="17" alt="開始日期">
				</a></td>
			</tr>
			<tr>
				<td>電話:</td>
				<td><input type="TEXT" name="empTel" size="45"
					value="<%=(employeeVO == null) ? "0932322331" : employeeVO.getEmpTel()%>" /></td>
			</tr>
			<tr>
				<td>性別:</td>
				<td><input type="TEXT" name="empSex" size="45"
					value="<%=(employeeVO == null) ? "M" : employeeVO.getEmpSex()%>" /></td>
			</tr>
			<tr>
				<td>職稱:</td>
				<td><input type="TEXT" name="empPos" size="45"
					value="<%=(employeeVO == null) ? "醫療人員" : employeeVO.getEmpPos()%>" /></td>
			</tr>
			<tr>
				<td>薪資:</td>
				<td><input type="TEXT" name="empSalary" size="45"
					value="<%=(employeeVO == null) ? "3200" : employeeVO
					.getEmpSalary()%>" /></td>
			</tr>
			<tr>
				<%
					java.sql.Date date_STRSQL = new java.sql.Date(
							System.currentTimeMillis());
				%>
				<td>到職日:</td>
				<td bgcolor="#CCCCFF"><input class="cal-TextBox"
					onFocus="this.blur()" size="9" readonly type="text"
					name="empArrDate"
					value="<%=(employeeVO == null) ? date_SQL : employeeVO
					.getEmpArrDate()%>">
					<a class="so-BtnLink" href="javascript:calClick();return false;"
					onmouseover="calSwapImg('BTN_date', 'img_Date_OVER',true);"
					onmouseout="calSwapImg('BTN_date', 'img_Date_UP',true);"
					onclick="calSwapImg('BTN_date', 'img_Date_DOWN');showCalendar('form1','empArrDate','BTN_date');return false;">
						<img align="middle" border="0" name="BTN_date"
						src="<%=request.getContextPath()%>/emp/images/btn_date_up.gif" width="22" height="17" alt="開始日期">
				</a></td>
			</tr>
			<tr>
				<%
					java.sql.Date date_ENDSQL = new java.sql.Date(
							System.currentTimeMillis());
				%>
				<td>離職日:</td>
				<td bgcolor="#CCCCFF"><input class="cal-TextBox"
					onFocus="this.blur()" size="9" readonly type="text" name="empOff"
					value="<%=(employeeVO == null) ? date_SQL : employeeVO.getEmpOff()%>">
					<a class="so-BtnLink" href="javascript:calClick();return false;"
					onmouseover="calSwapImg('BTN_date', 'img_Date_OVER',true);"
					onmouseout="calSwapImg('BTN_date', 'img_Date_UP',true);"
					onclick="calSwapImg('BTN_date', 'img_Date_DOWN');showCalendar('form1','empOff','BTN_date');return false;">
						<img align="middle" border="0" name="BTN_date"
						src="images/btn_date_up.gif" width="22" height="17" alt="開始日期">
				</a></td>
			</tr>
			<tr>
				<td>身分證字號:</td>
				<td><input type="TEXT" name="empID" size="45"
					value="<%=(employeeVO == null) ? "A169526255" : employeeVO.getEmpID()%>" /></td>
			</tr>
			<tr>
				<td>住址:</td>
				<td><input type="TEXT" name="empAdd" size="45"
					value="<%=(employeeVO == null) ? "桃園縣中壢市五權里2鄰中大路300號" : employeeVO.getEmpAdd()%>" /></td>
			</tr>
			<tr>
				<td>照片:</td>
				<td><input type="file" name="empPic" size="45"
					value="<%=(employeeVO == null) ? "full" : employeeVO.getEmpPic()%>" /></td>
			</tr>


		</table>
		<br> <input type="hidden" name="action" value="insert">
			 <input type="submit" value="送出新增">
	</FORM>
	
	<%@ include file="/menu2.jsp" %> 
	</c:if> 
	
</body>

</html>
