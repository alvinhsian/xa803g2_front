<%@ page contentType="text/html; charset=Big5"%>
<%@ page import="com.news.model.*"%>
<%@ page import="java.util.*"%>

<%@ page import="com.pow.model.*"%>
<jsp:useBean id="powSvc" scope="page" class="com.pow.model.PowService" />
<%
	NewsVO newsVO = (NewsVO) request.getAttribute("newsVO");

	PowVO powVO = powSvc.getOnePowByPKs((Integer)session.getAttribute("empNo"), 4008);
	List<PowVO> listPower = (List<PowVO>)session.getAttribute("list");
%>
<html>
<head>
<title>消息資料 - listOneNews.jsp</title>

<link rel="stylesheet" type="text/css" href="<%=request.getContextPath()%>/style.css">
<script type="text/javascript" src="<%=request.getContextPath()%>/js/jquery-1.11.0.min.js"></script>
<script src="<%=request.getContextPath()%>/menu.js"></script>		
</head>
<body bgcolor='white'>
	<%@ include file="/menu1.jsp" %> 

<c:if test="<%=listPower.contains(powVO)%>"> 

<table border='1' cellpadding='5' cellspacing='0' width='600'>
	<tr bgcolor='#CCCCFF' align='center' valign='middle' height='20'>
		<td>
		<h3>消息資料 - listOneNews.jsp</h3>
		<a href="<%=request.getContextPath()%>/backend/news/select_page.jsp"><img src="images/back1.gif" width="100" height="32" border="0">回首頁</a>
		</td>
	</tr>
</table>

<table border='1' bordercolor='#CCCCFF' width='600'>
	<tr>
		<th>消息編號</th>
		<th>消息主題</th>
		<th>消息類別</th>
		<th>消息內容</th>
		<th>照片</th>
		<th>發布時間</th>
		<th>員工編號</th>
	
	</tr>
	<tr align='center' valign='middle'>
		<td><%=newsVO.getNewsno()%></td>
		<td><%=newsVO.getNewstitle()%></td>
		<td><%=newsVO.getNewstype()%></td>
		<td><%=newsVO.getNewscontent()%></td>
		<td><img src="DBGifReader?newsno=${newsVO.newsno}"></td>
		<td><%=newsVO.getNewspotime()%></td>
		<td><%=newsVO.getEmpno()%></td>
	</tr>
</table>
</c:if>
	<%@ include file="/menu2.jsp" %> 

</body>
</html>
